﻿# Django
## Cấu trúc bên trong project Django.

    mysite/  
	    manage.py  
	    mysite/  
		    __init__.py  
		    settings.py  
		    urls.py  
		    wsgi.py
- project có tên thư mục **`mysite`** 
- **`manage.py`**: một CLI giúp bạn tương tác nhanh với Django
- Folder **_mysite_** bên trong thực chất là một **Python package**, và tên của nó sẽ là tên package bạn dùng để import trong code. 
	- VD: **`import mysite.urls`**
- **`mysite/__init__.py`**: File rỗng, có mục đích **biến folder này thành một Python package**
- **`mysite/settings.py`**: Các settings của project ở trong này.
- **`mysite/urls.py`**: Định nghĩa các URL của trang web.
- **`mysite/wsgi.py`**: Dùng khi deploy project của bạn.
***CHÚ Ý:*** 
	- **Khi đặt tên project tránh đặt những tên trùng với các từ khóa có sẵn trong Python** như `sys`, `os`, `django`…. để tránh bị xung đột.
	- không như PHP đặt code ở /var/www, Django sẽ tránh đặt ở các folder root, để bảo mật code, mỗi ứng dụng web **Django tự chạy một server cho chính nó, do đó có thể đặt code ở bất kỳ đâu.**
### App: 
- Mọi app viết bằng Django đều sẽ được xem là một Python package Và Django có sẵn công cụ để tạo structure căn bản cho một app.
- **App** và **Project** khác nhau ở đâu?
	-	**Một app sẽ chỉ thực hiện một chức năng nhất định**, ví dụ như hiển thị bài viết, quản lý user, hay tạo ứng dụng bình chọn v.v... Và **một Project sẽ chứa các app đó**, tạo thành một website. Và **một app cũng có thể dùng trong nhiều Project khác nhau**.
### Function `include()`
- Function **`include()`**  cho phép sử dụng các URLconf của các app con. Bất cứ khi nào Django xử lý **`include()`**, nó loại bỏ các phần khác của URL, và gửi string còn lại đến URLconf đã khai báo để xử lý.
### Function `path()`
- Function `path()`nhận **4 biến đầu vào** và 2 trong số đó là bắt buộc: **`route` và `view`** , 2 biến còn lại là `kwargs` và `name` .
	- **`route`**
		- `route` là string chứa pattern của URL. Khi xử lý một request, Django bắt đầu từ phần đầu tiên của `urlpatterns` , lần lượt đến cuối list đấy, so sánh cho đến khi tìm được pattern chính xác.
		- ***CHÚ Ý***: Các **pattern không tìm theo method POST hay GET, hoặc theo tên miền**. Ví dụ, một request tới `https://example.com/myapp` , URLconf sẽ tìm `myapp/` trong các file config URL. Và nếu request đến `https://www.example.com/myapp/?page=3 (https://www.example.com/myapp/?page=3)` , nó cũng tìm đến `myapp/` (Vì phần đằng sau là parameter).
	- **`view`**
		- Một khi Django đã tìm được một pattern match với URL được request, Django sẽ gọi một function view riêng, với một object **HttpRequest** như biến đầu vào đầu tiên và các phần đằng sau là các keyword argument.
	- **`kwagrs`** 
		- Một Python dictionary cho phép chúng ta truyền thêm vài thông tin đến một View.
	- **`name`**
		- Đặt tên cho từng URL rất tiện cho sau này khi dùng trong các View hoặc các Template.
## Model.
- **Thiết lập cơ sở dữ liệu**
	- Trong thư mục project chứa file `settings.py`, file này chứa các thông tin cấu hình server của bạn.
	- nhưng nếu muốn sử dụng CSDL khác thì trong file `settings.py`, tìm đến đối tượng `DATABASES` và thay đổi các giá trị sau:
		-   **`ENGINE`:** tên module dành cho từng CSDL, mặc định sử dụng SQLite
		    - `django.db.backends.sqlite3`  – cơ sở dữ liệu SQLite
		    - `django.db.backends.postgresql`  – cơ sở dữ liệu PostgreSQL
		     - `django.db.backends.mysql`  – cơ sở dữ liệu MySQL
			 - `django.db.backends.oracle`  – cơ sở dữ liệu Oracle
		-   **`NAME`**: tên CSDL, mặc định là file  `db.sqlite3`  được tạo ra ở thư mục gốc của server. Nếu không dùng CSDL SQLite thì phải tạo CSDL với tên trùng với NAME trong CSDL đang dùng (bằng câu lệnh  `CREATE DATABASE`  <name>)
- **`INSTALLED_APPS`** sẽ chứa tên tất cả các app của Django đang và sẽ dùng. Các app này có thể sử dụng trong nhiều project khác nhau, và có thể tự làm các package của mình và chia sẻ cho người khác sử dụng.
	- Các app mặc định của Django sẽ được đặt đầu tiên:
		-   django.contrib.admin: **Chính là trang Admin.**
		-   django.contrib.auth: **Dùng để Authentication**
		-  django.contrib.contenttypes: **Làm việc với các model**
		-   django.contrib.sessions: **Làm việc với  _session_**
		-   django.contrib.messages: **Dùng cho việc  _thông/cảnh báo_**
		-   django.contrib.staticfiles: **Dùng để quản lý  _static file_**

- Khi tạo project thì các bảng này không tự động được tạo trong file  `db.sqlite3`, để tạo các bảng này thì chúng ta chạy lệnh sau:
	```python sell 
	C:\Project\mysite>python manage.py makemigrations
	```
	- Lệnh `makemigrations` giúp tạo một thư mục **migration** để lưu lại các thay đổi của bảng cơ sở dữ liệu.
	```python sell 
	C:\Project\mysite>python manage.py migrate
	```
	- Lệnh  `migrate`  sẽ tìm các module được liệt kê trong list  `INSTALLED_APPS`  (trong file  `mysite/settings.py`) và tạo các bảng CSDL tương ứng.
### Thao tác với bảng.
- API thực ra là một cách để thử nghiệm các **queryset** của Django trên database. Vì Django sử dụng **ORM**, nên tất cả các dữ liệu của đều được dùng như một object (_theo đúng chất Python: **mọi thứ đều là object**_).
- có thể sử dụng trình shell mà Django cung cấp sẵn trong file `manage.py` để thực hiện.
	```Python shell 
	>>>from polls.models import Question, Choice 
	>>> Question.objects.all()
	```
	
	- Import 2 lớp `Question` và `Choice`. Phương thức **`Question.objects.all()`** liệt kê toàn bộ đối tượng `Question` đang có trong CSDL.
	```Python shell
	>>>from django.utils import timezone
	>>> q = Question(question_text = "What's new?" , pub_date =  timezone.now())
	```
	- Tạo đối tượng `Question` và thiết lập `question_text`, `pub_date` là các trường trong bảng `Question`, trong đó `pub_date`  ngày giờ hiện tại của máy tính, cần lấy thông tin này từ phương thức `timezone.now()` trong module `django.utils.timezone`.
	- **`id`** của mỗi model sẽ được tạo ra tự động với thứ tự tăng dần, nếu không khai báo trong code.
	- Vì tất cả các model đều là object, nên khi gọi Django sẽ trả về là object. Để tránh chỉ nhìn thấy **`<Question: Question object>`**, ta thêm
		```python 
		def __str__(self): 
			return self.question_text
		```
		để điều chỉnh chính xác thứ ta cần biết khi query. Ở đây thì sẽ hiển thị ra nội dung câu hỏi thay vì đống **`<Question: Question object>`**
	- có thể lọc các bản ghi theo thuộc tính bằng phương thức `filter()`.
		```Python shell
		>>> Question.objects.filter(id=1)
		[<Question: What's up?>]
		>>> Question.objects.filter(question_text__startswith='What')`
		[<Question: What's up?>]
		```
	- có thể lấy từng bản ghi đơn lẻ bằng phương thức `get()`, ở trên chúng ta lấy theo khóa chính `pk` (Primary Key).
		```Python shell
		>>> q = Question.objects.get(pk=1)
		```
	- có thể query các dữ liệu có quan hệ nữa. Dùng `__` khi `filter` là xong.
	- Đọc kỹ hơn về các Queryset của Django ở [đây](https://docs.djangoproject.com/en/2.0/topics/db/queries/) nhé.
## Hệ thống Admin 
- **Tạo user:**
	- Để tạo tài khoản thì chúng ta chạy file **`manage.py`** với tham số **`createsuperuser`**.
		```python shell
		C:\Project\mysite>python manage.py createsuperuser
		```
	 - Tiếp theo cung cấp **username, password và email**.
		 ```python shell
		Username: admin
		Email address: admin@example.com
		Password:
		Password (again):
		Superuser created successfully.
		```
	- Để truy cập vào trang admin **chỉ cần thêm /admin vào đường dẫn trang chủ** là được.
	- Để đăng ký các bảng (hay các mô hình) với admin thì chúng ta chỉ cần dùng phương thức `admin.site.register()` trong file `admin.py`.
		- ***Ví dụ***
			```python
			from django.contrib import admin
			from .models import Question, Choice 
			admin.site.register(Question)
			admin.site.register(Choice)
			```
## View.
- Django được thiết kế theo MVC, mặc dù nó không dùng các từ theo đúng nghĩa như vậy. 
	- **Model** thì rõ ràng rồi, là cách mà ta cấu trúc các dữ liệu sẽ dùng trong tương lai. 
	- **View** trong Django thì thực tế là phần Controler. Nó sẽ thực hiện các phần logic của website. 
	- **Template** mới là View trong MVC. Template trong Django có sẵn trong nó các logic riêng, dù đơn giản (vì mục đích template không phải để xử lý logic, mà để hiển thị). 
	
	**=>** Nên có thể gọi Design Patter của Django là MTV.
- **Trong Django**, toàn bộ nội dung sẽ được xử lý ở trong các view. **Mỗi view là một Function** — **hoặc một method (trường hợp bạn dùng class-based views)**. Django sẽ tự động dùng view tương ứng bằng cách phân tích URL được truy cập.
- Một URL pattern đơn giản chỉ là một mẫu chung để chúng ta biết được client đang muốn làm cái gì. 
	- Ví dụ:  `/blog/ngày/tháng/năm/tên-bài-viết`

	Và chúng ta sẽ dựa vào form trên để chạy các function tương ứng trong  **View**.
- ***Ví Dụ:***
	- ở file `polls/views.py` và thêm view như sau.
		```python 
		from django.http import HttpResponse
		
		def detail(request, question_id):  
	    return HttpResponse("You're looking at question %s." %question_id)
		```
	- Chúng ta kết nối các view này với các URL bằng cách thêm vào trong file `polls/urls.py` như sau:
		```python
		from django.urls import path  
		from . import views  
		
		urlpatterns = [  
			# ex: /polls/5/  
			path('<int:question_id>/', views.detail, name='detail'),  
		]
		```
		- ***Kết  quả***
			- Thử `localhost:8000/polls/34` . Và function `detail()` sẽ hoạt động, ta sẽ thấy `You’re looking at question 34.` hiển thị trên trang web
		- Khi một người gửi request đến một trang của website
			- ***Ví dụ:*** `/polls/34/` 
		- Django sẽ load **`mysite.urls`** dựa vào biến **`ROOT_URLCONF`** trong settings. Nó sẽ tìm trong list các URL pattern: **`urlpatterns`**. Sau khi tìm thấy `polls/` , sẽ strip phần `polls/` , lấy phần còn lại là `34/` và tìm tiếp trong `polls.urls` . Và sẽ match với `<int:question_id>/` trong đó
		- kết quả là gọi function `detail()` tương ứng như sau:
			```python 
			detail(request=<HttpRequest object>, question_id=34)
			```
		- **`question_id=34`** là từ **`<int:question_id>`** . Dùng 2 dấu ngoặc **`<>`** để thể hiện rằng đó là phần biến mà ta sẽ dùng để query ra kết quả tương ứng. **`<int:`** sẽ giới hạn các biến có thể nhận.
## Template.
-  Để giảm hard-coded trong view nên tách ra 2 phần hiển thị và phần logic riêng với nhau, nên ta cần **Template**.
- Hãy tạo một folder tên `tempaltes` trong `polls` . Django sẽ tự tìm templates trong đó.
	- ### ***Chú ý:*** 
		- Thực tế **hãy tạo `templates` ở ngoài cùng, cùng chỗ với `manage.py` và config trong `settings.py`** để Django tìm các file template ở đó. Sau đó, **mỗi app sẽ có folder template của riêng chúng**. Nếu dự án phát triển lên với nhiều app, ta sẽ thấy cách này dễ quản lý hơn rất nhiều.
- Và bây giờ view sẽ đẩy sang template một dictionary chứa các biến cần thiết để hiển thị. Dictionary đó còn gọi là context data. 
## `render()`
- Vì công đoạn load template, truyền context data và return một HttpResponse bị lặp lại quá nhiều, Django đã tạo một function riêng để thực hiện công đoạn đó:
	```python
	from django.shortcuts import render from .models import Question  
	def index(request):  
	    latest_question_list = Question.objects.order_by('-pub_date')[:5]  
	    context = {'latest_question_list': latest_question_list}  
	    return render(request, 'polls/index.html', context)
	```
- Giúp ngắn code hơn và không cần phải import thêm **`loader`** và **`HttpResponse`** .
## `get_object_or_404()`
```python
from django.http import Http404  
from django.shortcuts import render from .models import Question  
    try:  
        question = Question.objects.get(pk=question_id)  
    except Question.DoesNotExist:  
        raise Http404("Question does not exist")  
    return render(request, 'polls/detail.html', {'question': question})
```
- Với đoạn code ở trên view sẽ raise Http404 exception nếu câu hỏi không tồn tại.
- Tạo thêm một file `detail.htm`, hiển thị nội dung chỉ cần:
	```python
	{{ question }}
	```
	### `get_object_or_404()`
	- Cũng trả về Http404 khi không tìm thấy object như trên nhưng ở đây django đã tạo một shortcut cho quy trình này.
	- File `view.py` khi dùng `get_object_or_404()` sẽ như sau.
		```python
		from django.shortcuts import get_object_or_404, render 
		from .models import Question  
		def detail(request, question_id):  
		    question = get_object_or_404(Question, pk=question_id)  
		    return render(request, 'polls/detail.html', {'question': question})
	    ```
 ## `get_list_or_404()`
 - Cũng cùng chức năng với `get_object_or_404()`, **nhưng** khác ở chỗ nó **dùng `filter()`** chứ **không phải `get()`** . Và **trả về Http404 nếu list rỗng.**
 - ***Ví dụ:***
	 - Lấy tất cả các đối tượng có `published = True` trong `MyModel`
		```python
		from django.http import Http404

		def my_view(request):
		    my_objects = list(MyModel.objects.filter(published=True))
		    if not my_objects:
		        raise Http404("No MyModel matches the given query.")
		```
	- Dùng **`get_list_or_404()`** sẽ như sau.
		```python
		from django.shortcuts import get_list_or_404
		
		def my_view(request):
		    my_objects = get_list_or_404(MyModel, published=True)
	    ```
	  
## URL động.
```django
<li>
	<a href="/polls/{{ question.id }}/">{{ question.question_text }}</a>
</li>
```
- Vấn đề ở đây nếu như ta có quá nhiều đường dẩn, việc thay đổi rất khó khắn. tuy nhiên hàm `path()` trong module `polls.urls` đã giúp ta loại bỏ được các vấn đề đó bằng cách sử dụng `{% url %}`, code trên sẽ như sau.
```django
<li>
	<a href="{% url 'detail' question.id %}">{{ question.question_text }}</a>
</li>
```
- Bằng cách này chúng ta có thể tham chiếu đến đối tượng `url` trong file `urls.py`, và khi nào cần thay đổi URL mới thì chúng ta chỉ cần thay đổi trong file `urls.py` là được:
	```python
	# the 'name' value as called by the {% url %} template tag
	path('<int:question_id>/', views.detail, name='detail'),
	```
- Và việc sửa đổi sẽ trở nên dể dàng hơn.
	```python
	# added the word 'specifics'
	path('specifics/<int:question_id>/', views.detail, name='detail'),
	```
## File Tĩnh.

- Ngoài nội dung HTML được sinh ra bởi server thì một ứng dụng web còn cần đến các file bổ sung khác, chẳng hạn như các file hình ảnh, Javascript, CSS… Trong Django thì các file này được gọi là file tĩnh.
### Tùy biến file.
- Tạo một thư mục có tên là `static` trong thư mục app, Django sẽ tự động tìm các file tĩnh trong thư mục này.
- Trong thư mục `static` ta lại tạo một thư mục khác với **tên cùng tên của app** và tạo file CSS, JS....
- Để sử dụng file tĩnh thêm `{% load staticfiles %}` vào file cần import file. Chỉ cần lấy biến `static` là có thể lấy được đường dẫn tuyệt đối đến thư mục này.
- **VD:**
	- tạo file css với nội dung và đường dẩn như sau:
	`polls/static/polls/style.css`
		```css
		li a {
			color:green
		}
		```
	- Tiếp theo chúng ta cần sửa lại file `index.html` .
		`polls/templates/polls/index.html`
		```html
		{% load staticfiles %}
		<link rel="stylesheet" type="text/css" href="{% static 'polls/style.css' %}"/>
		```
	- Dòng `{% load staticfiles %}` sẽ tự động gán đường dẫn đến thư mục `mysite/polls/static/` vào một biến có tên `static` do Django tự đặt, từ đó chỉ cần lấy biến `static` là có thể lấy được đường dẫn tuyệt đối đến thư mục này.
		```html
		<link rel="stylesheet" type="text/css" href="{% static 'polls/style.css' %}"/>
		```
	- Sau đó chỉ cần lấy biến `static` trong cặp thẻ `{% %}` ra và gắn thêm đường dẫn đến các file css, js… .
## Form có sẵn của Django.
- Nếu việc code các form bằng tay quá mệt mỏi thì Django cung cấp cho chúng ta lớp `Form` để chúng ta có thể tạo form ngay từ code Python và có thể nhận request và trả response về cho người dùng.
- ***Ví Dụ***
	```python
	#user_auth/forms.py tạo form với đường dẩn 
	from django import forms

	class RegisterForm(forms.Form):
		username=forms.CharField(label='Username', max_length=100)
		password=forms.CharField(widget=forms.PasswordInput)
		email = forms.EmailField(label='Email')
	```
	- Các lớp dùng để tạo form được kế thừa từ lớp `django.forms.Form`.
	- Bên trong lớp này chúng ta cũng khai báo các trường là các đối tượng `Field`, nhưng các đối tượng `Field` này không giống như `Field` khi tạo model, `Field` ở đây là để tạo form HTML còn field bên model là để tạo bảng CSDL.
	- **Vì HTML có nhiều thẻ element có công dụng chung nhưng lại hiển thị dữ liệu khác nhau**, chẳng hạn như một thẻ `<input>` có thể dùng để nhập tên, số điện thoại, password… **do đó Django cung cấp cho mỗi đối tượng `Field` một đối tượng `Widget` để chúng ta có thể chỉ định loại text nào hiển thị cái gì.**
		```python
		password = forms.CharField(widget=forms.PasswordInput)
		```
	- **Ở templates:**
		```django
		<form action=""method="post">
			{% csrf_token %}
			{{ form }}
			<input type="submit"value="Submit"/>
		</form>
		```
		- Chúng ta có thể tham chiếu đến phần tử `form` trong list context mà chúng ta sẽ khai báo trong các hàm view bên dưới, và Django sẽ tự động tạo các thẻ `<label>` và `<input>` cho chúng ta.
		```django
		{{ form }}
		```
	- Ở View:
		- chúng ta chỉ import lớp từ module `forms.py` mà chúng ta viết ở trên. Sau đó tạo một đối tượng và đưa vào làm phần tử của list context (tham số thứ 3 trong hàm `render()).`
		```python
		def register(request):
			if request.method=='POST':
				response=HttpResponse()
				response.write("<h1>Thanks for registering</h1></br>")
				response.write("Your username: "+request.POST['username'] + "</br>")
				response.write("Your email: " + request.POST['email'] + "</br>")
				return response
			registerForm=RegisterForm()
			return render(request,'user_auth/register.html', {'form':registerForm})
		```
## Form từ model.
- Cũng giống như cách tổ chức và tạo như Form có sẵn của Django
- Hai class Model của chúng ta kế thừa **`django.forms.ModelForm`** thay vì `django.forms.Form`.
- Chúng ta định nghĩa model được dùng trong lớp nội `Meta`, thuộc tính **`model` sẽ tạo các field tương ứng từ lớp _Model_ tương ứng**, thuộc tính **`fields` sẽ chọn các trường nào được dùng**, 
- nếu chúng ta **không khai báo thuộc tính `fields` thì mặc định Django sẽ dùng tất cả các thuộc tính có trong lớp _Model**,_ ngoại trừ thuộc tính `id` nếu bạn không khai báo thuộc tính khóa chính.  
- Ngoài ra lớp Meta còn có thuộc tính **`exclude`**, **thuộc tính này trái ngược với thuộc tính** **`fields`**, tức là **thuộc tính này sẽ quy định các trường nào không được phép sử dụng**.
	```python
	class Meta:  
	    model = Post  
	    fields = ('title', 'content', 'date_time',)
	```
### Lưu dữ liệu.
- Lớp **`ModelForm`** có một phương thức tên là **`save()`**, p**hương thức này tạo mới hoặc lưu một đối tượng Model vào cơ sở dữ liệu giống như phương thức `save()` bên các lớp _Model_**
- Khi khởi tạo các đối tượng `ModelForm` thì chúng ta có thể truyền các đối tượng model có sẵn vào hàm khởi tạo, nếu chúng ta chỉ truyền vào không thôi thì Django sẽ tạo mới một đối tượng trên CSDL, nếu chúng ta truyền vào và ghi rõ truyền với tham số là `instance` thì Django sẽ cập nhật dữ liệu trong đối tượng đó
	- ***VD:***
		- Tạo đối tượng mới từ đối tượng `POST` được gửi lên và lưu vào CSDL.
			```python
			f = ArticleForm(request.POST)
			new_article = f.save()
			```
		- Tạo một đối tượng `ArticleForm,` truyền dữ liệu từ `POST` vào rồi truyền cập nhật trên đối tượng `a` chứ không tạo mới.
			```python 
			a = Article.objects.get(pk=1)
			f = ArticleForm(request.POST, instance=a)
			f.save()
			```
### Tùy chỉnh lớp Field.
- Mặc dù tạo form bằng cách code từ đầu với HTML hoặc dùng lớp `django.forms.Form` sẽ cho bạn quyền điều khiển nhiều hơn nhưng không có nghĩa là `ModelForm` không cho phép bạn tùy chỉnh các lớp field có sẵn.
- Ví dụ: Tùy chỉnh kiểu hiển thị thì chúng ta dùng thuộc tính `widgets` có trong lớp nội `Meta.`
	```python
	from django.forms import ModelForm, Textarea  
	from myapp.models import Author  
	  
	class AuthorForm(ModelForm):  
	    class Meta:  
	        model = Author  
	        fields = ('name', 'title', 'birth_date')  
	        widgets = {  
	            'name': Textarea(attrs={'cols': 80, 'rows': 20}),  
			}
	```
	- **Mặc định** lớp **`CharField`** sẽ **hiển thị thẻ `<input type="text">`**, nhưng đoạn code trên sẽ chỉ định `CharField` hiển thị `<input type="textarea">`.
- Ta cũng có thể tùy chỉnh lại các thuộc tính như `label`, `help_text` trong lớp nội `Meta` 
	```python 
	from django.utils.translation import ugettext_lazy as _  
	  
	class AuthorForm(ModelForm):  
	    class Meta:  
	        model = Author  
	        fields = ('name', 'title', 'birth_date')  
	        labels = {  
	            'name': _('Writer'),  
	  }  
	        help_texts = {  
	            'name': _('Some useful help text.'),  
	  }
	```
- Ta cũng có thể quy định các thuộc tính phải dùng lớp field do bạn định nghĩa thông qua thuộc tính `field_classes` trong lớp `Meta`
	```python
	from django.forms import ModelForm  
	from myapp.models import Article  
	  
	class ArticleForm(ModelForm):  
	    class Meta:  
	        model = Article  
	        fields = ['pub_date', 'headline', 'content', 'reporter', 'slug']  
	        field_classes = {  
	            'pub_date': MyDateTimeField,  
	  }  
	  
	class MyDateTimeField():  
	    # ...  
	  pass
	```
### Thừa kế Form
- `ModelForm` cũng chỉ là một lớp Python, do đó ta có thể cho thừa kế và mở rộng các trường hoặc các phương thức của chúng.
	```python
	class EnhancedArticleForm(ArticleForm):  
	    def clean_pub_date(self):  
	        #...   
			pass
	```
	- Lớp `EnhancedArticleForm` kế thừa từ lớp `ArticleForm` nên sẽ có tất cả các thuộc tính và phương thức của lớp `ArticleForm`. lớp `EnhancedArticleForm` có thêm phương thức `clean_pub_date()` của riêng nó.
- Tương tự với các lớp Model, **các lớp form cũng có thể thừa kế cả lớp nội Meta**:
	```python
	class RestrictedArticleForm(EnhancedArticleForm):  
	    class Meta(ArticleForm.Meta):  
	        exclude = ('body',)
	```
	- Lớp `RestrictedArticleForm` kế thừa từ lớp `EnhancedArticleForm` ngoại trừ lớp này không sử dụng thuộc tính `body`, dùng với hàm **`exclude`**
- ***LƯU Ý:***  **là khi sử dụng đa thừa kế thì lớp `Meta` của lớp con chỉ thừa kế từ lớp `Meta` của lớp cha đầu tiên trong danh sách mà thôi.**
### Hàm factory.
- Nếu bạn quá “lười” để ngồi định nghĩa lại một lớp kế thừa từ `ModelForm`, sau đó ngồi khai báo các thuộc tính `fields `, `exclude` hay lớp `Meta` thì **Django cũng cung cấp một hàm cho phép bạn tạo một lớp `ModelForm` một cách “cấp tốc” là hàm `modelform_factory()`**.
	```python
	from django.forms import modelform_factory  
	from myapp.models import Book  
	  
	BookForm = modelform_factory(Book, fields=("author", "title"))
	```
- Ta cũng có thể đưa các thuộc tính chỉnh sửa vào ngay trong hàm `modelform_factory()`:
	```python
	from django.forms import Textarea  
	Form = modelform_factory(Book, form=BookForm, widgets={"title": Textarea()}, fields=['name'])
	```
## Request.
- View nhận **HttpRequest** object làm tham số đầu tiên và trả về **HttpResponse** objects. Django sử dụng request và response objects để lưu trữ, vận chuyển các trạng thái xuyên suốt ứng dụng.
### Request.
- Khi người dùng yêu cầu một trang web, Django sẽ tạo một **HttpRequest** object chứa các thông tin về **metadata của request**. Sau đó object này được truyền tới view thích hợp **( như là tham số đầu tiên của view )**.
- ***THUỘC TÍNH***
	- **request.method**
		- Một chuổi đại diện cho HTTP method của request (và được viết hoa). Với function base view không có sự liên kết với với HTTP method ta có thể sử dụng thuộc tính này để xác định phần code thực hiện cho mỗi method khác nhau.
	- **request.body** 
		- Chứa **raw HTTP request body** như là một chuỗi byte. Nó sẽ giúp chúng ta thuận tiện hơn trong việc xử lí data với nhiều cách khác nhau hơn là những HTML forms thông thường: binary images, XML payload, … 
		- Để xử lí các forms data thông thường sử dụng **request.POST**. Ta cũng có thể đọc data từ **HttpRequest** sử dụng file-like interface: **request.read()**.
	- **request.GET**
		- Một đối tượng giống dictionary chứa tất cả các tham số của HTTP GET.
		- **Ví dụ:** để lấy giá trị của tham số name trên url:
		```python
		name = request.GET.get("name","Default value")	
		```
	- **request.POST**
		- Tương tự như **request.GET** nhưng nó chứa các tham số của HTTP POST, và ***CHÚ Ý* rằng nó không kèm thông tin của file upload** (thông tin này nằm ở trong **HttpRequest.FILES**). 
	- **request.COOKIES**
		- Là một python dictionary chứa tất cả cookies. keys và value là string.
	- **request.FILES** 
		- Là một **`dictionary-like object`**  chứa tất cả file đã được upload. Mỗi key ở FILE là name của input:
			```html
			<input type="file" name=""/>
			```
		- Mỗi value của FILES là một đối tượng UploadedFile . 
		- ***CHÚ Ý*** rằng FILES sẽ chỉ chứa data nếu request method là POST và **`<form >`** được post có **`enctype=”multipart/form-data”`**. Ngược lại FILES sẽ là **`dictionary-like object`**  rỗng.
- ***PHƯƠNG THỨC*** 
	- **request.get_host()**  
		- Trả về originating host của request sử dụng **`HTTP_X_FORWARDED_HOST`** (nếu như **`USE_X_FORWARDED_HOST`** được kích hoạt) và **`HTTP_HOST`** headers. Nếu như chúng không được cung cấp giá trị thì phương thức sẽ kết hợp 2 giá trị **`SERVER_NAME`** và **`SERVER_PORT`** để trả về.
***VÍ DỤ:*** **“127.0.0.1:8000”**

	- **request.is_secure()**  
		- **Trả về True nếu request là an toàn** (request được thực hiện với giao thức **https**)

	- **request.is_ajax()**  
		- Trả về True nếu request là  **XMLHttpRequest**  (bằng cách kiểm tra giá trị **`HTTP_X_REQUESTED_WITH`** header cho giá trị string **`XMLHttpRequest`**).

	- **request.read(size=None), request.readline(), request.readlines(), request.xreadlines(), request.__iter__()**  
		- Tập các phương thức implementing file-like interface cho nhiệm vụ đọc từ một  **HttpRequest**  instance.
