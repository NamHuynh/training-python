﻿# Python advanced

 ### **1. Class và cách khai báo class trong Python**
```python 
class Person:  
	# thuộc tính 
	name = "Huỳnh Quang Nam"; 
	age = 22; 
	male = True  
	# phương thức  
	def setName(self, name): 
		self.name = name
```
- **Chú ý**: từ khóa `self`. Thì từ khóa `self` ở đây dùng để thể hiện lại chính class đang chứa nó, và dựa vào nó thì chúng ta có thể truy cập vào các phần tử đang có trong class hiện tại. **( self [python] == this [js] )**
- **Khởi tạo class.**
	- ```variableName = className()```
		- `variableName`  là biến mà bạn muốn thể hiện lại đối tượng.
		-  `className`  là class mà bạn muốn khởi tạo.
		```python
		#truy cap den thuoc tinh 
		object.propertyName 
		#truy cap den phuong thuc 
		object.methodName()
		```
		-  `object`  là biến thể hiện lại object.
		-   `propertyName`  là tên thuộc tính mà bạn muốn truy xuất.
		-   `methodName`  là tên phương thức mà bạn muốn truy xuất.
### **2. Constructor và destructor trong Python**
- **Constructor.**
	```python 
	class className:  
		def __init__(self, param1, param2,...):  
	```
	-   `className`  là tên của class.
	-   `param1, param2,...`  là các tham số chúng ta muốn nhận khi kèm khi 		khởi tạo class.
- **Deconstructor.**
	```python 
	def __del__(self):  
		# code
	```
### **3.  Kế thừa class trong Python**
- **Kế thừa trong Python**
	```python 
	class className(inherit1, inherit2,...):  
	#code
	```
	- `inherit1, inherit2,...` là tên của các class mà bạn muốn kế thừa.
- **Ghi đè ( override )**
	```python
	class Foo: 
		name = 'Foo'  
		def getName(self): 
			print("Class: Foo") 
			
	class Bar(Foo): # kế thừa từ lớp Foo
		name = 'Bar' # cùng tên thuộc tính khác VALUE 
		def getName(self): #cùng tên phương thức
			print("Class: Bar") # khác NỘI DUNG THỰC HIỆN
			
	print(Foo().name) 
	Foo().getName() 
	
	print(Bar().name) 
	Bar().getName() 
	
	# Ket qua:  
	# Foo  
	# Class: Foo  
	#  
	# Bar  
	# Class: Bar
	```
- **Super()**
	- Trong trường hợp ở **class con** mà bạn **muốn sử dụng đến các thành phần trong class cha** thì bạn phải sử dụng hàm super theo cú pháp sau:
		```python 
		# Đối với thuộc tính. 
		super().variableName 
		# Đối với phương thức. 
		super().methodName()
		```
- **Đa kế thừa trong Python.**
	```python
	class Third(First, Second):  
		def getThird(self): 
			print("Class Third")
	```
### **4.  Phạm vi truy cập thuộc tính và phương thức trong Python OOP**
- **public**
	- Tên của thành phần **KHÔNG được bắt đầu** bằng ký tự `_` mà **PHẢI bắt đầu bằng chữ cái**.
- **protected**
	- Tên của thành phần **PHẢI được bắt đầu** bằng 1 ký tự `_`.
- **private**
	- Tên cả thành phần **PHẢI được bắt đầu** bằng 2 ký tự `__`.
### **5. Abstraction trong Python**
- Để có thể khai báo được một abstract class trong Python, **thì class này bắt buộc phải được kế thừa** từ một **ABC** (**A**bstract **B**ase **C**lasses) của Python
	```python 
	from abc import ABC
	```
- **Cú pháp khai báo abstract class:**
	```python 
	class ClassName(ABC):  # chú ý có ABC
		# code
	```
- Khai báo phương thức abstract trong Python.
	```python 
	from abc import ABC, abstractmethod 

	class ClassName(ABC):  
		# khai bao phuong thuc truu tuong  
		@abstractmethod  
		def methodName(self):
			#code
	```
	-    `@abstractmethod`  là **bắt buộc**, đây là cú pháp khai báo cho Python biết phía dưới là phương thức trừu tượng.
	-   `methodName`  là tên của phương thức trừu tượng.
- Một class kế thừa từ abstract class thì **bắt buộc** phải **khai báo lại các các phương thức trừu tượng có trong abstract mà nó kết thừa**.
### **6. Iterator trong Python**
- Iterator trong Python phải thực hiện hai phương thức đặc biệt là  ```__iter__()```  và  ```__next__()```  gọi chung là giao thức **iterator (Iterator Protocol)**

	-   Phương thức  ```__iter__```  **trả về chính đối tượng iterator**. Phương thức này được yêu cầu cài đặt cho cả đối tượng "iterable" và iterator để có thể sử dụng các câu lệnh for và in.
	-   Phương thức ```__next__``` **trả về phần tử tiếp theo**. Nếu không còn phần tử nào nữa thì sẽ có lỗi StopIteration xảy ra.
- **Iterable object**  là một đối tượng sau khi sử dụng các phương thức sẽ trả về một iterator
	- **Ex:** như Chuỗi, List, Tuple.

- **Iter()** là một hàm dựng sẵn trong Python nhận đầu vào là một đối tượng iterable và trả về kết quả là một iterator.
- **Xây dựng trình vòng lặp Iterator trong Python**
	```python
	class PowTwo: 
		def __init__(self, max = 0): 
			self.max = max 
		def __iter__(self): 
			self.n = 0 
			return self 
		def __next__(self): 
			if self.n <= self.max: 
				result = 2 ** self.n 
				self.n += 1 
				return result
			else: 
	raise StopIteration
	```
	```python
	# KẾT QUẢ THỰC HIỆN
	>>> a = PowTwo(4) 
	>>> i = iter(a) 
	>>> next(i) 
	1 
	>>> next(i) 
	2 
	>>> next(i) 
	4 
	>>> next(i) 
	8 
	>>> next(i) 
	16 
	>>> next(i) 
	Traceback (most recent call last): 
	... 
	StopIteration
	```
- **Ưu điểm** của việc sử dụng Iterator lặp là chúng **tiết kiệm tài nguyên**.Nhận lại được các giá trị mà **không phải lưu trữ trên toàn bộ hệ thống số ở bộ nhớ**.
### **7. Generator trong Python**
- **Generator là cách đơn giản để tạo ra iterator. Một generator là một hàm trả về một chuỗi kết quả thay vì một giá trị duy nhất.**
	-  **Để tạo generator trong Python, sử dụng từ khóa  `def`**  giống như khi định nghĩa một hàm. Trong generator, dùng câu lệnh  `yield`  ***để trả về các phần tử thay vì câu lệnh  `return`  như bình thường.***

	- Nếu **một hàm chứa ít nhất một  `yield`**  (**có thể có nhiều  `yield`  và thêm cả  `return`**) thì chắc chắn đây **là một hàm generator**. Trong trường hợp này, cả  `yield`  và  `return`  sẽ trả về các giá trị từ hàm.

	- ***ĐIỀU ĐẶC BIỆT*** ở đây là  `return`  **sẽ chấm dứt hoàn toàn một hàm**, còn  `yield` ***sẽ chỉ tạm dừng các trạng thái bên trong hàm và sau đó vẫn có thể tiếp tục khi được gọi trong các lần sau***
		```python
		def yrange(n): 
			i = 0 while i < n: 
				yield i 
				i += 1
		```
-  **Biểu thức generator**
	- Giống như Hàm  Lambda trong Python tạo một hàm vô danh trong Python, generator cũng tạo một biểu thức generator vô danh. **Cú pháp tương tự như cú pháp của list comprehension**, **nhưng dấu ngoặc vuông được thay thế bằng dấu ngoặc tròn.**
		```python
		myList = [1,3,4,5]
		a = (x * x for x in range(10))  
		print(sum(a))
		# KẾT QUẢ TRẢ VỀ
		# 285
		```
	- **Nên sử dụng generator trong Python**
		- **Đơn giản hóa code, dễ triển khai:**
			```python
			class PowTwo:
			     def __init__(self, max = 0):
			         self.max = max
			     def __iter__(self):
			         self.n = 0
			         return self
			     def __next__(self):
			         if self.n > self.max:
			             raise StopIteration
			         result = 2 ** self.n
			         self.n += 1
			         return result
			```
			**thay vì dùng code trên ta dùng generator:**
			```python
			def PowTwoGen(max = 0):
			     n = 0
			     while n < max:
			         yield 2 ** n
			         n += 1
			```
			- **Sử dụng ít bộ nhớ**
				- **Generator** sẽ sử dụng ít bộ nhớ hơn vì **chúng chỉ thực sự tạo kết quả khi được gọi tới**, sinh ra một phần tử tại một thời điểm, đem lại hiệu quả nếu chúng ta không có nhu cầu duyệt nó quá nhiều lần.
### **8. Closure trong Python**
- **Biến nonlocal** được sử dụng trong hàm lồng nhau nơi mà phạm vi cục bộ không được định nghĩa. Nói dễ hiểu thì biến ***nonlocal không phải biến local cũng không phải là biến global***, khai báo một biến là nonlocal khi muốn sử dụng nó ở phạm vi rộng hơn local, nhưng chưa đến mức global.
- **Định nghĩa hàm Closure trong Python**
	- **Closure** thường được tạo ra bởi hàm lồng nhau, có thể hiểu là những hàm ghi nhớ không gian nơi mà nó tạo ra.
	- **CHÚ Ý:** ***closure được tạo ra bởi hàm lồng nhau nhưng không phải hàm lồng nhau nào cũng là closure***. Closure chỉ **được tạo ra khi hàm con truy cập đến những biến cục bộ trong scope được đóng bởi hàm cha**.
	```python
	def print_msg(msg):
	 # Hàm bê ngoài
	 
	     def printer():
		 # Hàm lồng nhau
	         print(msg)
	 
	     return printer
	 
	 another = print_msg("Hello")
	 another()
	```
- **Điều kiện để có Closure**
	- Để tạo closure trong Python, các tiêu chí cần phải đáp ứng bao gồm:
		-   ***Có một hàm lồng nhau*** (hàm được định nghĩa bên trong một hàm khác).
		-   ***Hàm lồng nhau phải tham chiếu đến một giá trị được xác định trong hàm kèm theo***.
		-   ***Hàm kèm theo phải trả kết quả về hàm lồng nhau***.

- Khi cần định nghĩa một class chỉ với vài phương thức, ***closure có thể được sử dụng như một giải pháp thay thế nhẹ nhàng hơn lập trình hướng đối tượng***. Tuy nhiên là khi các thuộc tính và phương thức nhiều lên, sử dụng lập trình hướng đối tượng sẽ là giải pháp tốt hơn.
- **Ví dụ:**  về việc sử dụng closure thích hợp hơn phương pháp lập trình hướng đối tượng với việc xác định lớp và tạo các đối tượng
	```python 
	def make_multiplier_of(n):
	     def multiplier(x):
	         return x * n
	     return multiplier
	 
	 times3 = make_multiplier_of(3)
	 # He so 3
	 
	 times5 = make_multiplier_of(5)
	 # He so 5
	 
	 print(times3(9))
	 # Output: 27
	 
	 print(times5(3))
	 # Output: 15
	 
	 print(times5(times3(2)))
	 # Output: 30
	```
### **9. Decorator trong Python**	
- Decorator là một hàm ***nhận tham số đầu vào là một hàm khác và mở rộng tính năng cho hàm đó mà không thay đổi nội dung của nó.***
-  **Điều kiện để có Decorator**
	-  trong Python, **hàm cũng là đối tượng**
		```PYTHON 
		def first(msg):
		     print(msg)    
		 
		 first("Hello")
		 
		 second = first
		 second("Hello")
		```
	- Những hàm lấy hàm khác làm tham số đầu vào được gọi là **hàm bậc cao** (higher-order functions)
		```python
		def inc(x):
		     return x + 1
		 
		def dec(x):
		     return x - 1
		 
		def operate(func, x):
		     result = func(x)
		     return result
		```
		Chúng ta gọi hàm như sau.
		```python
		>>> operate(inc,3)
		 4
		 >>> operate(dec,3)
		 2
		```
	- **Decorator là một hàm có thể nhận các hàm khác, cho phép bạn chạy một số đoạn code trước hoặc sau hàm chính mà không thay đổi kết quả.**
		```python
		def make_pretty(func):
		     def inner():
		         print("I got decorated")
		         func()
		     return inner
		 
		 def ordinary():
		     print("I am ordinary")

		#Chạy code trong Python shell:
		>>> ordinary()
		 I am ordinary
		 
		 >>> # Thử hàm decorate trong hàm ordinary
		 >>> pretty = make_pretty(ordinary)
		 >>> pretty()
		 I got decorated
		 I am ordinary
		```
	- Có thể sử dụng ký hiệu  `@`  cùng với tên của hàm decorator và đặt nó lên trên định nghĩa của hàm được decorator. 
	- Ví dụ:

		```python
		@make_pretty
		 def ordinary():
		     print("I am ordinary")
		```

		tương đương với:

		```python
		def ordinary():
		     print("I am ordinary")
		ordinary = make_pretty(ordinary)
		```
