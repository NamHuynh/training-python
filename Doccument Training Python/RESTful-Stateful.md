﻿# RESRful.
- **API** (**A**pplication **P**rogramming **I**nterface) **là một tập các quy tắc và cơ chế mà theo đó, một ứng dụng hay một thành phần sẽ tương tác với một ứng dụng hay thành phần khác**. API có thể trả về dữ liệu mà bạn cần cho ứng dụng của mình ở những kiểu dữ liệu phổ biến như **JSON** hay **XML**.

- **REST** (**RE**presentational **S**tate **T**ransfer) **không phải là một chuẩn hay một giao thức, đây là một cách tiếp cận, một kiểu kiến trúc để viết API**. 
	- **Một web service** là một tập hợp các giao thức và chuẩn được sử dụng cho mục đích trao đổi giữa ứng dụng và hệ thống. Web service dựa trên các kiến trúc REST được biết như RESTful webservice . Những webservice này sử dụng phương thức HTTP để triển khai các định nghĩa kiến trúc REST. 
	- **Một RESTful web service thường được định nghĩa một URI** (kiểu như đường dẫn), Uniform Resource Identifier như một service (dịch vụ).
	- **Các ứng dụng sử dụng kiểu thiết kế REST thì được gọi là RESTful**. Trên thực tế ta hay sử dụng thuật ngữ REST thay cho RESTful và ngược lại.

- **RESTful API** là **một tiêu chuẩn dùng trong việc thết kế các thiết kế API cho các ứng dụng web để quản lý các resource**. RESTful là một trong những kiểu thiết kế API được sử dụng phổ biến. 
	- Trọng tâm của REST quy định cách sử dụng các HTTP method (**GET**, **POST**, **PUT**, **DELETE**...) và cách định dạng các URL cho ứng dụng web để quản lý các resource. **RESTful không quy định logic code ứng dụng và không giới hạn bởi ngôn ngữ lập trình ứng dụng.**
- **Hoạt động RESTful.**
	- **REST hoạt động chủ yếu dựa vào giao thức HTTP**.
		-   **GET** (SELECT): Trả về một Resource hoặc một danh sách Resource.
		-   **POST** (CREATE): Tạo mới một Resource.
		-   **PUT** (UPDATE): Cập nhật thông tin cho Resource.
		-   **DELETE** (DELETE): Xoá một Resource.
	- Những phương thức (hoạt động) trên thường được gọi là  **CRUD** (**C**reate, **R**ead, **U**pdate, **D**elete)
# Statefull
### **Stateless**
- **Stateless là thiết kế không lưu dữ liệu của client trên server**. Có nghĩa là sau khi client gửi dữ liệu lên server, server thực thi xong, trả kết quả thì “quan hệ” giữa client và server bị “**cắt đứt**” – server không lưu bất cứ dữ liệu gì của client. 

### **Stateful**
- **Stateful là một thiết kế ngược với stateless**, server cần lưu dữ liệu của client, điều đó đồng nghĩa với việc ràng buộc giữa client và server vẫn được giữ sau mỗi request (yêu cầu) của client. Data được lưu lại phía server có thể làm đầu vào (input parameters) cho lần kế tiếp, hoặc là dữ kiện dùng trong quá trình xử lý hay phục vụ cho bất cứ nhu cầu nào phụ thuộc vào bussiness (nghiệp vụ) cài đặt.
