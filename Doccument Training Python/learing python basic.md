﻿## PyThon basic
### **1. Python cơ bản**
-   **Viết  câu  lệnh  trên  nhiều  dòng**
	-   \
	-   ()
	-   {}
	-   []
-   **Thụt  lề  trong  Python**
-  **Chú  thích, bình  luận  trong Python**
	-   ký  tự **#** để  bắt  đầu  một  chú  thích
	-   3 dấu  nháy  đơn **' ' '** hoặc  nháy  kép **" " "**
-   **Đợi  người  dùng  hành  động**
	```python 
	input("\n\nNhấn phím Enter để thoát!") 
	```
- **Biến  trong Python**
	-   Gán  giá  trị  cho  biến    
	-   Gán  nhiều  giá  trị
	    -   **Ex:**  hoa, la, canh = "Hồng", 3, 5.5
	    -   **Ex:**  hoa, la, canh = 3
    

- **Kiểu  dữ  liệu  số  trong Python**
    -   Hàm  **`type()`** để  kiểm  tra  xem  biến  hoặc  giá  trị  thuộc  lớp  số  nào 
    -  Ép  kiểu  dữ  liệu  trong Python
		-   **float(data)**  chuyển  đổi sang kiểu  số  thực.
		-   **int(data,base)**  chuyển  đổi sang kiểu  số, trong  đó base là  kiểu  hệ  số  mà  các  bạn  muốn  chuyển  đổi sang (tham  số  này  có  thể  bỏ  trống).
		-   **str(data)** chuyển  đổi sang dạng  chuỗi.
	    -   **complex(data)**  chuyển  đổi sang kiểu  phức  hợp.
	    -   **tuple(data)**  chuyển  đổi sang kiểu Tuple.
	    -   **dict(data)**  chuyển  đổi sang kiểu Dictionary.
	    -   **hex(data)**  chuyển  đổi sang hệ 16.
	    -   **oct(data)**  chuyển  đổi sang hệ 8.
	    -   **chr(data)**  chuyển  đổi sang dạng  ký  tự.
	  -   Hàm **isinstance()** để  kiểm  tra  xem  chúng  có  thuộc  về  một class cụ  thể  nào  không.
			-  **Ex:** **`print( isinstance ( b, complex ) )`**
- **Chuỗi  trong python**
		- Fomat  chuỗi.  **`print("%type" %(binding))`**
    

- **Truy  cập  tới  từng  giá  trị  của  chuỗi.**
     **`stringName[index]`**
	-   `stringName`  là  tên  của  biến  chứa  chuỗi, hoặc  chuỗi.
	-   `index`  là  vị  trí  của  ký  tự  bạn  muốn  lấy ra. Index này  hỗ  trợ  chúng ta truy  xuất  được  cả 2 chiều  của  chuỗi  nếu:
	-   Tính  từ  đầu  thì  nó  bắt  đầu  từ 0.    
	-   Tính  từ  cuối  thì  nó  bắt  đầu  từ -1.
	
	**`stringName[start:end]`**
	-   **`stringName`**  là  tên  của  biến  chứa  chuỗi, hoặc  chuỗi.
	-   `start`  là  vị  trí  của  ký  tự  bắt  đầu  lấy, nếu  để  trống  start  thì  nó  sẽ  lấy  từ 0.    
	-   `end`  là  vị  trí  kết  thúc (nó  sẽ  lấy  trong  khoảng  từ  start  đến < end), nếu  để  trống  end  thì  nó  sẽ  lấy  đến  hết  chuỗi.
-   **List**
**`list[start:end]`**
	-   `list`  là  tên  của  biến  chứa list.  
	-   `start`  là  ví  trí  bắt  đầu  lấy ra list con. Nếu  để  trống  thì  nó  sẽ  lấy  từ  đầy list.   
	-   `end`  là  vị  trí  kết  thúc. Nếu  để  trống  thì  nó  sẽ  lấy  đến  phần  tử  cuối  cùng  của list.
    

- **Delete**
	Để  xóa  một  hoặc  nhiều  phần  tử  trong list thì  các  bạn  cần  truy  cập  đến  phần  tử  cần  xóa  và  dùng  hàm  **del**  để  xóa. Và  sau  khi  chúng ta xóa  phần  tử  trong list thì index của list sẽ  được  cập  nhật  lại.
	-	**Ex:** `del name[2]`
- **Toán tử**
	- **Toán Tử khai thác**
		- Giả sử: 
			- a = 4,
			-  	b = [1,5,7,6,9]
		
	| Toán Tử | Chú Thích | Ví Dụ |
	|--|--|--|
	| in | Nếu 1 đối số thuộc một tập đối số nó sẽ trả về True và ngược lại | a in b //False |
	| not in | Nếu 1 đối số **không** thuộc một tập đối số nó sẽ trả về True và ngược lại | a not in b //True |
	- **Toán tử xác thực.**
		- Giả sử: 
			- a = 4,
			- b =5
		
	| Toán Tử | Chú Thích | Ví Dụ |
	|--|--|--|
	| is| Toán tử này sẽ trả về True nếu a == b và ngược lại | a is b //False |
	| not is | Toán tử này sẽ trả về True nếu a != b và ngược lại | a is not b //True |

### **2. Condition**
-   **Lệnh pass**
	- Chúng ta thường sử dụng lệnh này như một **placeholder**
	- Giả sử, có **một vòng lặp**, hoặc **một hàm**, nhưng chưa biết nên xây dựng nó như thế nào, chưa biết nên code sao cho tối ưu và muốn để lại làm sau. **Nhưng** hàm, lệnh đó **không thể có một khối lệnh rỗng**, trình biên dịch sẽ báo lỗi, **vì thế**, **chỉ cần sử dụng lệnh pass để xây dựng một khối lệnh rỗng**, lúc này trình biên dịch sẽ hiểu và không "phàn nàn" gì bạn nữa
		```python 
		# for
		for val in sequence:
			pass
		# function 
		def function(args): pass
		# class
		class example: pass
		```
-   **Kết hợp while với else**
	```python 
	dem = 0
	while dem < 3:
		print("Đang ở trong vòng lặp while")
		dem = dem + 1
	else:
		print("Đang ở trong else")
		
	# Đang ở trong vòng lặp while
	# Đang ở trong vòng lặp while
	# Đang ở trong vòng lặp while
	# Đang ở trong else 
	```

-   **NOTE**
	-   Trong **Python không  hỗ  trợ  cú  pháp  swtich-case** mà  thay  vào  đó  bạn  có  thể  sử  dụng  kiểu  dữ  liệu dictionary để  giải  quyết.
	-   **Ex:**  
		``` python
			a = 'hai' 
			dic = { 'mot': 1, 'hai': 2, 'ba': 3, }
		    print(dic.get(a,'khong  ro'))`
		    # Ket Qua: 2
		```
### **3. Function**
-   **Phạm vi của  biến  trong  hàm.**
	-   **Chú  ý :**
		```python
		a = [5, 10, 15]
		def  change(a):
			a[0] = 1000
			print(a)
		change(a)
		#KQ: [1000, 10, 15]
		print(a)
		#KQ: [1000, 10, 15]
		```
-   **Truyền  vô  số  tham  số  vào  hàm.**
-   **Ex:**
	```python
	def  get_sum(*num):
		tmp = 0
	# duyet  cac  tham so
		for  i  in num:
			tmp += i
		return tmp
			
	result = get_sum(1, 2, 3, 4, 5)
	print(result)
	# KQ: 15
	```
 - **Hàm lambda trong Python**
	  ```python
	lambda tham_so: bieu_thuc
	```
	- Hàm Lambda có **thể có nhiều tham số**`code`nhưng **chỉ có 1 biểu thức**. Biểu thức sẽ được đánh giá và trả về. Hàm Lambda có thể được sử dụng ở bất cứ nơi nào đối tượng hàm được yêu cầu.
	- Ex: 
		```python
		nhan_doi = lambda a: a * 2
		# Kết quả: 20
		```

- ### map().
	- **duyệt qua tất cả các phần tử của một hoặc nhiều list**, dictionary hoặc tương tự như thế, sử dụng đơn giản với cú pháp như sau:
		```python
		map(function, iterable1, iterable2 ,...)
		```
	- **VD**: nhân đôi giá trị của tất cả các phần tử trong list.
		```python
		def mutiply(x):
		    return x * x
		result = map(mutiply, [1, 2, 3, 4])
		print(list(result)) # [1, 4, 9, 16]
		```
- ### filter().
	- duyệt qua các phần tử trong list, dict,... nhưng **khác với map là hàm này sẽ chỉ trả về những giá trị mà điều kiện trong function** chấp nhận (có nghĩa là True).
	- **VD**:  `filter`  các giá trị không chia hết cho 2.
		```python
		result = filter(lambda x: x % 2, [1, 3, 5, 6, 7, 9, 22])
		print(list(result))  # [1, 3, 5, 7, 9]
		```
### **4. File**
 -   **Input và  Đọc  ghi file trong Python**
		- **Input.**
			```python
			input(something)
			```
		- **Ex:**
			```python
			print("Hello guy!")
			age = input("How old are you? ")
			print("age: " + age)
			```

- **Đọc  ghi file.**
	- **Mở file.**
		```python 
		open(filePath, mode, buffer)
		```
	-  **Đọc file.**
		```python	
    	fileObject.read(length)
    	```
		-   **fileObject**  là  đối  tượng  mà  chúng ta thu  được  khi  sử  dụng  hàm  **open()**.
		-   **length**  là dung lượng  của  dữ  liệu  mà  chúng ta muốn  đọc, nếu  để  trống  tham  số  này  thì  nó  sẽ  đọc  hết file hoặc  nếu file lớn  quá  thì  nó  sẽ  đọc  đến  khi  giới  hạn  của  bộ  nhớ  cho  phép.

	- **Ex:**
		```python
		# mo file
		file = open('readme.md')
		# doc file data = file.read();
		# dong file  file.close()
		# in du lieu doc duoc
		print(data)
		```
- **Ghi file.** 
	```python 
	 fileObject.write(data)
	```
-   **fileObject**  là  đối  tượng  mà  chúng ta thu  được  khi  sử  dụng  hàm  **open().**
-   **data** là  dữ  liệu  mà  chúng ta muốn  ghi  vào  trong file.
    

	- **Ex:**
		```python
		# mo file o che do ghi
		file = open('readme.md','w')
		# ghi file
		file.write('Vu Thanh Tai - toidicode.com')
		# dong file
		file.close()
		```

