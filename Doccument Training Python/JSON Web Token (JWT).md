﻿# JSON Web Token (JWT)
- **JWT** là một **phương tiện đại diện cho các yêu cầu chuyển giao giữa hai bên Client – Server** , các thông tin trong chuỗi **JWT** **được định dạng bằng JSON** . Trong đó chuỗi Token phải  **gồm 3 phần** là **header** , phần **payload** và phần **signature** được ngăn bằng dấu “.”
### Header.
- **Header** là phần chứa **kiểu dữ liệu** , và **thuật toán sử dụng để mã hóa ra chuỗi JWT**	.
	```JSON
	{
		"typ":  "JWT",
		"alg":  "HS256"
	}
	```
-   **“typ” (type)** chỉ ra rằng đối tượng là một **JWT**.
-   **“alg” (algorithm)** xác định thuật toán mã hóa cho chuỗi là **HS256**.
### Payload.
- **Payload** là phần chứa các thông tin mình muốn đặt trong chuỗi **Token** như **username** , **userId** , **author** , …
	```JSON
	{
		"user_name":  "admin",
		"user_id":  "1513717410",
		"authorities":  "ADMIN_USER",
		"jti":  "474cb37f-2c9c-44e4-8f5c-1ea5e4cc4d18"
	}
	```
- ***CHÚ Ý:*** không nên đặt quá nhiều thông tin trong chuỗi **payload** vì nó sẽ ảnh hưởng đến độ trể khi **Server** phải xác nhận một **Token** quá dài.
### Signature.
- Phần chử ký này sẽ được tạo ra bằng cách mã hóa phần **header** , **payload** kèm theo một chuỗi **secret (khóa bí mật)**
	```python
	data = base64urlEncode(header) + "." + base64urlEncode(payload)
	signature = Hash(data, secret)
	```
- **base64UrlEncoder :** thuật toán mã hóa **header** và **payload**.
- Đoạn code trên sau khi mã hóa **header** và **payload** bằng thuật toán **base64UrlEncode** ta sẽ có chuỗi như sau.
	```javascript
	// header
	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9
	// payload
	eyJhdWQiOlsidGVzdGp3dHJlc291cmNlaWQiXSwidXNlcl9uYW1lIjoiYWRtaW4iLCJzY29wZSI6WyJyZWFkIiwid3JpdGUiXSwiZXhwIjoxNTEzNzE
	```
- Sau đó mã hóa 2 chuỗi trên kèm theo **secret (khóa bí mật)** bằng thuật toán **HS256** ta sẽ có chuỗi **signature** như sau:
	``` python shell
	9nRhBWiRoryc8fV5xRpTmw9iyJ6EM7WTGTjvCM1e36Q
	```
### Kết hợp 3 chuỗi trên lại ta sẽ có được một chuỗi JWT hoàn chỉnh.
	```
	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsidGVzdGp3dHJlc291cmNlaWQiXSwidXNlcl9uYW1lIjoiYWRtaW4iLCJzY29wZSI6WyJyZWFkIiwid3JpdGUiXSwiZXhwIjoxNTEzNzE.9nRhBWiRoryc8fV5xRpTmw9iyJ6EM7WTGTjvCM1e36Q
	```
## Nên dùng JSON Web Token khi nào ?.
-   **Authentication**: Đây là trường hợp phổ biến nhất thường sử dụng JWT. Khi người dùng đã đăng nhập vào hệ thống thì những request tiếp theo từ phía người dùng sẽ chứa thêm mã JWT. Điều này cho phép người dùng được cấp quyền truy cập vào các url, service, và resource mà mã Token đó cho phép. Phương pháp này không bị ảnh hưởng bởi Cross-Origin Resource Sharing (CORS) do nó không sử dụng cookie.
-   **Trao đổi thông tin**: JSON Web Token là 1 cách thức khá hay để truyền thông tin an toàn giữa các thành viên với nhau, nhờ vào phần  **signature**  của nó. Phía người nhận có thể biết được người gửi là ai thông qua phần  **signature**. Và chữ ký được tạo ra bằng việc kết hợp cả phần header, payload lại nên thông qua đó ta có thể xác nhận được chữ ký có bị giả mạo hay không.

