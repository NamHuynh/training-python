# Generated by Django 3.0.3 on 2020-03-09 09:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('poll', '0002_choice_votes'),
    ]

    operations = [
        migrations.CreateModel(
            name='Login',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('user_name', models.CharField(max_length=200)),
                ('password', models.CharField(max_length=200)),
            ],
        ),
    ]
