from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import render
from .models import Question, Login

# Create your views here.
def login(request):
    return render(request, "poll/login.html")


def checkLogin(request):
    user_name = request.POST['username']
    pass_word = request.POST['password']
    return HttpResponse(user_name + " " + pass_word)

def index(request):
    listName = ["Huynh Quang Nam", "Tran Quan Lam", "Hoang Nguyen Luat"]
    return render(request, "poll/index.html", {'listName': listName})


def viewList(request):
    listQuestion = Question.objects.all()
    return render(request, "poll/content_page.html", {'listQuestion': listQuestion})


def detailView(request, question_id):
    q = Question.objects.get(pk=question_id)
    return render(request, "poll/detail_question.html", {'qs': q})


def vote(request, question_id):
    q = Question.objects.get(pk=question_id)
    try:
        re = request.POST["choice"]
        c = q.choice_set.get(pk=re)
    except:
        return HttpResponse("lỗi không có choice nghe chú... check lại nghe!!!!")
    c.votes += 1
    c.save()
    print(q)
    return render(request, "poll/result.html", {'rq': q})

