from django.urls import path

from . import views

urlpatterns = [
    path('choice/<int:question_id>', views.vote, name='vote'),
    path('detail/<int:question_id>', views.detailView, name='detail'),
    path('list/', views.viewList, name='view-list'),
    path('check/', views.checkLogin, name='check-login'),
    path('login/', views.login, name='login'),
    path('', views.index, name='index'),

]