from django.urls import path

from . import views

urlpatterns = [
    path('acc/', views.GetAllApiView.as_view(), name='acc'),
    path('loginView/', views.login, name='login'),
]