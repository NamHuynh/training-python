import hashlib

from django.shortcuts import render
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from django.http import HttpResponse
from .models import Login
from .serializers import GetAllAccount, Account
# Create your views here.

def login(request):
    return render(request, "Login/loginView.html")

class GetAllApiView(APIView):

    def get(self, request):
        list_acc = Login.objects.all()
        dataAcc = GetAllAccount(list_acc, many=True)
        return Response(data=dataAcc.data, status=status.HTTP_200_OK)

    # tạo tài khoản
    # def post(self, request):
    #     dataPost = {'user_name': request.data['username'], 'pass_word': request.data['password']}
    #     mydata = Account(data=dataPost)
    #     if not mydata.is_valid():
    #         return Response("data is not validate ", status=status.HTTP_400_BAD_REQUEST)
    #     userName = mydata.data['user_name']
    #     passWord = mydata.data['pass_word']
    #     datafilter = Login.objects.filter(user_name=userName)
    #     if datafilter.count() == 0:
    #         passTranMD5 = hashlib.md5(passWord.encode()).hexdigest()
    #         Login.objects.create(user_name=userName, pass_word=passTranMD5)
    #         return Response(data={'isCreate': True}, status=status.HTTP_200_OK)
    #     return render(request, "Login/loginView.html")


    # đăng nhập
    def post(self, request):
        dataPost = {'user_name': request.data['username'], 'pass_word': request.data['password']}
        mydata = Account(data=dataPost)
        if not mydata.is_valid():
            return Response("data is not validate ", status=status.HTTP_400_BAD_REQUEST)
        userName = mydata.data['user_name']
        passWord = mydata.data['pass_word']
        passTranMD5 = hashlib.md5(passWord.encode()).hexdigest()
        datafilter = Login.objects.filter(user_name=userName, pass_word=passTranMD5)
        if datafilter.count() == 0:
            return render(request, "Login/loginView.html")
        return Response({'isLogin': True}, status=status.HTTP_200_OK)


    # test demo
    # def post(self, request):
    #     mydata = Account(data=request.data)
    #     if not mydata.is_valid():
    #         return Response("data is not validate ", status=status.HTTP_400_BAD_REQUEST)
    #     userName = mydata.data['user_name']
    #     passWord = mydata.data['pass_word']
    #     dataCreate = Login.objects.create(user_name=userName, pass_word=passWord)
    #     return Response(data=dataCreate.id, status=status.HTTP_200_OK)