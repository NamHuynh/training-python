from rest_framework import serializers
from .models import Login

class GetAllAccount(serializers.ModelSerializer):

    class Meta:
        model = Login
        fields = ('user_name', 'pass_word',)


class Account(serializers.Serializer):
    user_name = serializers.CharField(max_length=50, allow_blank=False, allow_null=False)
    pass_word = serializers.CharField(max_length=50, allow_blank=False, allow_null=False)
