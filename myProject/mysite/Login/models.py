from django.db import models


# Create your models here.
class Login(models.Model):
    user_name = models.CharField(max_length=50, blank=False, null=False)
    pass_word = models.CharField(max_length=50, blank=False, null=False)

    def __str__(self):
        return self.user_name
