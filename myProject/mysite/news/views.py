from django.shortcuts import render
from django.http import HttpResponse
from .form import PostForm


# Create your views here.

def new(request):
    return render(request, "news/index.html")


def post_News(request):
    a = PostForm()
    return render(request, "news/add_post.html", {'f':a})

def save_news(request):
    if request.method == 'POST':
        p = PostForm(request.POST)
        if p.is_valid():
            p.save()
            return HttpResponse("<h1 style='color: green'>save</h1>")
        else:
            return HttpResponse("<h1 style='color: red'>don't validate !</h1>")
    else:
        return HttpResponse("<h1 style='color: red'>don't method POST !</h1>")