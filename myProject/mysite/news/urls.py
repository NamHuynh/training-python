from django.urls import path
from . import views
app_name = 'news'
urlpatterns = [
    path('', views.new, name="news"),
    path('add/', views.post_News, name="add"),
    path('save/', views.save_news, name="save"),
]
